﻿using Fls.Core.DataInterface;
using Fls.Core.Domain;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Fls.Core.Processor
{
    public class CartPlaceOrderRequestProcessorTests
    {
        private readonly CartPlaceOrderRequestProcessor _processor;
        private readonly Mock<IProductsRepository> _productRepository;

        /// <summary>
        /// Products to be used for the tests
        /// </summary>
        private readonly List<Product> _products = new()
        {
            new Product
            {
                Id = 1,
                Name = "Vase",
                Price = 1.2
            },
            new Product
            {
                Id = 2,
                Name = "Big mug",
                Price = 1,
                //Discount = new Discount { Quantity = 2, Price = 1.5 }
            },
            new Product
            {
                Id = 3,
                Name = "Napkins pack",
                Price = 0.45,
                //Discount = new Discount { Quantity = 3, Price = 0.9 }
            }
        };

        /// <summary>
        /// Discounts to be used for the tests
        /// </summary>
        private readonly Dictionary<int, Discount> _discounts = new()
        {
            {
                2,
                new Discount(1.5, 2)
            },
            {
                3,
                new Discount(0.9, 3)
            }
        };

        public CartPlaceOrderRequestProcessorTests()
        {
            _productRepository = new Mock<IProductsRepository>();

            _productRepository.Setup(x => x.GetCurrentDiscounts(It.IsAny<IEnumerable<int>>()))
                .Returns(_discounts);

            _processor = new CartPlaceOrderRequestProcessor(_productRepository.Object);
        }

        [Fact]
        public void ShouldReturnZeroWithEmptyCart()
        {
            var cart = new Cart();
            var request = new CartPlaceOrderRequest
            {
                Cart = cart
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            Assert.NotNull(result);
            Assert.Equal(0, result.TotalPrice);
        }

        [Fact]
        public void ShouldThrowExceptionIfRequestIsNull()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => _processor.PlaceOrder(null));
            Assert.Equal("request", exception.ParamName);
        }

        [Fact]
        public void ShouldReturnSingleProductPrice()
        {
            Product product = _products[0];

            var request = new CartPlaceOrderRequest
            {
                Cart = new Cart(product, 1)
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            Assert.NotNull(result);
            Assert.Equal(product.Price, result.TotalPrice);
        }

        [Fact]
        public void ShouldReturnProductPricesWithoutAnyDiscount()
        {
            var cart = new Cart(
                new Dictionary<Product, int>()
                {
                    { _products[0], 1},
                    { _products[1], 1},
                    { _products[2], 1}
                }
            );

            var request = new CartPlaceOrderRequest
            {
                Cart = cart
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            Assert.NotNull(result);
            Assert.Equal(_products.Sum(p => p.Price), result.TotalPrice);
        }

        [Fact]
        public void ShouldReturnSingleProductPriceWithExactDiscountQuantity()
        {
            Product product = _products[1];
            Discount discount = _discounts[product.Id];
            var request = new CartPlaceOrderRequest
            {
                Cart = new Cart(product, discount.Quantity)
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            Assert.NotNull(result);
            Assert.Equal(discount.Price, result.TotalPrice);
        }

        [Theory]
        [InlineData(2, 0)]
        [InlineData(2, 1)]
        [InlineData(2, 3)]
        [InlineData(2, 7)]
        [InlineData(3, 0)]
        [InlineData(3, 1)]
        [InlineData(3, 3)]
        [InlineData(3, 7)]
        public void ShouldReturnSingleProductPriceWithDiscount(int productId, int quantity)
        {
            Product product = _products.First(p => p.Id == productId);
            Discount discount = _discounts[productId];

            var request = new CartPlaceOrderRequest
            {
                Cart = new Cart(product, quantity)
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            double expectedTotalSum = Cart.CalculateDiscount(quantity, product.Price, discount);
            Assert.NotNull(result);
            Assert.Equal(expectedTotalSum, result.TotalPrice);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(7)]
        public void ShouldReturnMultipleProductsPriceWithDiscounts(int quantity)
        {
            double expectedTotalPrice = 0;
            var cart = new Cart();
            foreach (var product in _products)
            {
                Discount discount = _discounts.FirstOrDefault(p => p.Key == product.Id).Value;

                cart.Add(product, quantity);
                expectedTotalPrice += Cart.CalculateDiscount(quantity, product.Price, discount);
            }

            var request = new CartPlaceOrderRequest
            {
                Cart = cart
            };

            CartPlaceOrderResult result = _processor.PlaceOrder(request);

            Assert.NotNull(result);
            Assert.Equal(expectedTotalPrice, result.TotalPrice);
        }

    }
}
