﻿using Fls.Core.DataInterface;
using Fls.Core.Domain;
using System;
using System.Collections.Generic;

namespace Fls.DataAccess
{
    public class ProductsRepository : IProductsRepository
    {
        public IEnumerable<Product> GetAllProducts()
        {
            throw new NotImplementedException();
        }

        public IDictionary<int, Discount> GetCurrentDiscounts(IEnumerable<int> products)
        {
            throw new NotImplementedException();
        }
    }
}
