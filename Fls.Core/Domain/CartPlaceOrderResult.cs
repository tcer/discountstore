﻿namespace Fls.Core.Domain
{
    public class CartPlaceOrderResult
    {
        public double TotalPrice { get; set; }
    }
}