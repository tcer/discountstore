﻿using System.Collections.Generic;
using System.Linq;

namespace Fls.Core.Domain
{
    /// <summary>
    /// A cart contains a list of products and their respective quantities 
    /// </summary>
    public class Cart
    {
        private Dictionary<Product, int> _products; // Dictionary <product, quantity>
        public Dictionary<Product, int> Products
        {
            get
            {
                if (_products == null)
                {
                    _products = new Dictionary<Product, int>();
                }
                return _products;
            }
            set => _products = value;
        }

        public Cart() { }

        public Cart(Dictionary<Product, int> products)
        {
            if (products != null)
            {
                foreach (KeyValuePair<Product, int> product in products)
                {
                    AddOrUpdate(product.Key, product.Value);
                }
            }
        }

        public Cart(Product product, int quantity)
        {
            AddOrUpdate(product, quantity);
        }

        public void Add(Product product, int quantity)
        {
            AddOrUpdate(product, quantity);
        }

        /// <summary>
        /// Add a product and its quantity to the cart
        /// If the product is already present, the quantity is updated
        /// </summary>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        private void AddOrUpdate(Product product, int quantity)
        {
            if (Products.ContainsKey(product))
            {
                int newQuantity = Products[product] = quantity;

                if (newQuantity == 0)
                {
                    Products.Remove(product);
                }
                else
                {
                    Products[product] += quantity;
                }
            }
            else
            {
                Products.Add(product, quantity);
            }
        }


        public double GetTotalPrice(IDictionary<int, Discount> discounts)
        {
            double totalPrice = 0;
            if (_products != null && _products.Any())
            {
                foreach (KeyValuePair<Product, int> product in _products)
                {
                    int quantity = product.Value;
                    double normalPrice = product.Key.Price;

                    if (discounts.ContainsKey(product.Key.Id) && quantity > 0)
                    {
                        totalPrice += CalculateDiscount(quantity, normalPrice, discounts[product.Key.Id]);
                    }
                    else
                    {
                        totalPrice += product.Key.Price * quantity;
                    }
                }
            }

            return totalPrice;
        }

        /// <summary>
        /// Calculates the total price for one product, regarding the quantity and the discount
        /// </summary>
        /// <param name="quantity"></param>
        /// <param name="normalPrice"></param>
        /// <param name="discount"></param>
        /// <returns></returns>
        public static double CalculateDiscount(int quantity, double normalPrice, Discount discount)
        {
            if (discount == null)
            {
                discount = new Discount(normalPrice, 1);
            }
            int numberOfDiscounts = quantity / discount.Quantity;
            double sumDiscountedPrices = numberOfDiscounts * discount.Price;
            double sumNotDicountedPrices = (quantity % discount.Quantity) * normalPrice;

            return sumDiscountedPrices + sumNotDicountedPrices;
        }
    }
}
