﻿namespace Fls.Core.Domain
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }


        public bool Equals(Product product) => product.Id.Equals(Id);
        public override bool Equals(object o) => Equals(o as Product);
        public override int GetHashCode() => Id.GetHashCode();
    }
}
