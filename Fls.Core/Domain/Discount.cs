﻿namespace Fls.Core.Domain
{
    public class Discount
    {
        public int Quantity { get; set; }
        public double Price { get; set; }

        public Discount(double price, int quantity)
        {
            Price = price;
            Quantity = quantity;
        }
    }
}
