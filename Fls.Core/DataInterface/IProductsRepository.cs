﻿using Fls.Core.Domain;
using System.Collections.Generic;

namespace Fls.Core.DataInterface
{
    public interface IProductsRepository
    {
        /// <summary>
        /// Retrieves all the products
        /// </summary>
        /// <returns></returns>
        IEnumerable<Product> GetAllProducts();

        /// <summary>
        /// Retrieves the current discounts related to a list of product ids
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        IDictionary<int, Discount> GetCurrentDiscounts(IEnumerable<int> products);
    }
}
