﻿using Fls.Core.DataInterface;
using Fls.Core.Domain;
using System;
using System.Linq;

namespace Fls.Core.Processor
{
    public class CartPlaceOrderRequestProcessor
    {
        private readonly IProductsRepository _productsRepository;
        public CartPlaceOrderRequestProcessor(IProductsRepository productsRepository)
        {
            _productsRepository = productsRepository;
        }

        public CartPlaceOrderResult PlaceOrder(CartPlaceOrderRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            // Before the order is executed, we retrieve the last discounts
            var discounts = _productsRepository.GetCurrentDiscounts(request.Cart.Products.Keys.Select(p => p.Id));

            var result = new CartPlaceOrderResult
            {
                TotalPrice = request.Cart.GetTotalPrice(discounts)
            };

            return result;
        }
    }
}